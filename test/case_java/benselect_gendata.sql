CREATE TABLE table_id (
  id int primary key,
  pk text
);

insert into table_id select seq, md5(seq::text) from generate_series(1, 100000) as seq;


CREATE TABLE bench_select (
  pk text primary key,
  t  text
);

CREATE OR REPLACE FUNCTION random_string(INTEGER)
RETURNS TEXT AS
$BODY$
SELECT array_to_string(
    ARRAY (
        SELECT substring(
            '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
            FROM (ceil(random()*62))::int FOR 1
        )
        FROM generate_series(1, $1)
    ), 
    ''
)
$BODY$
LANGUAGE sql VOLATILE;

insert into bench_select select pk, random_string(128) from table_id;

