#!/bin/bash

javac benchselect.java
export CLASSPATH=.:./lib/postgresql-42.2.5.jar
java -Xms128m -Xmx256m benchselect "$@"

